# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#password = 'pass123'
#1.upto(5) do |i|
#  Student.create(
#      email: "student-#{i}@uni.edu",
#      password: password,
#      password_confirmation: password
#  )
#end
#Question.create(
#        quiz_id: 2,
#        title: "Which of the following is true of cooperative scheduling?",
#        chapter: 5,
#        row_order: 1
#)
#Course.create!(
#	title: "Betriebssysteme",
#	semester: "SS17",
#    teacher_id: 1
#)

# Quiz.create!(
# 	title: "Scheduling",
# 	course_id: 1
# )

# QuizSession.create!(
# 	access_key: "h34j02hg",
# 	quiz_id: 1
# )

# Question.create!(
#         quiz_id: 1,
#         title: "Which of the following is true of a blocking system call?",
#         row_order: 2
# )

 Answer.create!(
	 question_id: 2,
	 title: "The application continues to execute its code when the call is issued.",
	 correct: false
 )

 Answer.create!(
     question_id: 2,
     title: "The call returns immediately without waiting for the I/O to complete.",
     correct: false
 )

 Answer.create!(
     question_id: 2,
     title: "The execution of the application is suspended when the call is issued.",
     correct: true
 )

 Answer.create!(
     question_id: 2,
     title: "Blocking application code is harder to understand than nonblocking application code.",
     correct: false
 )
 
 Answer.create!(
     question_id: 1,
     title: "It requires a timer.",
     correct: false
 )
 
 Answer.create!(
     question_id: 1,
     title: "A process keeps the CPU until it releases the CPU either by terminating or by switching to the waiting state.",
     correct: true
 )
 
 Answer.create!(
     question_id: 1,
     title: "It incurs a cost associated with access to shared data.",
     correct: false
 )
 
 Answer.create!(
     question_id: 1,
     title: "A process switches from the running state to the ready state when an interrupt occurs.",
     correct: false
 )
 
 