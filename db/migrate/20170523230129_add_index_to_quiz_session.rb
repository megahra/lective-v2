class AddIndexToQuizSession < ActiveRecord::Migration[5.0]
  def change
    add_index :quiz_sessions, :access_key, unique: true, name: 'index_quiz_sessions_on_access_key'
  end
end
