class CreateQuizSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :quiz_sessions do |t|
      t.string :access_key
      t.integer :current_question_index

      t.timestamps
    end
  end
end
