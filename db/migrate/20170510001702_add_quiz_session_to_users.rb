class AddQuizSessionToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :quiz_session, foreign_key: true
  end
end
