class AddPhaseToQuizSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :quiz_sessions, :phase, :int, :default => 0
  end
end
