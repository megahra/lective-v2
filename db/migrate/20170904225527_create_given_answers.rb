class CreateGivenAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :given_answers do |t|
      t.references :student, index:true, references: :users
      t.references :quiz_session, index:true, foreign_key: true
      t.references :question, index:true, foreign_key: true
      t.string :answer, :default => "[No answer given]"
      t.boolean :correct, :default => false

      t.timestamps
    end
  end
end
