class AddQuizToQuizSessions < ActiveRecord::Migration[5.0]
  def change
    add_reference :quiz_sessions, :quiz, foreign_key: true
  end
end
