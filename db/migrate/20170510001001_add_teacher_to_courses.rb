class AddTeacherToCourses < ActiveRecord::Migration[5.0]
  def change
    add_reference :courses, :teacher, references: :users
  end
end
