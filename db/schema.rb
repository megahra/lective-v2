# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171014192729) do

  create_table "answers", force: :cascade do |t|
    t.string   "title"
    t.boolean  "correct"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_answers_on_question_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string   "title"
    t.string   "access_key"
    t.string   "semester"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "teacher_id"
    t.index ["teacher_id"], name: "index_courses_on_teacher_id"
  end

  create_table "courses_students", id: false, force: :cascade do |t|
    t.integer "course_id"
    t.integer "student_id"
    t.index ["course_id"], name: "index_courses_students_on_course_id"
    t.index ["student_id"], name: "index_courses_students_on_student_id"
  end

  create_table "given_answers", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "quiz_session_id"
    t.integer  "question_id"
    t.string   "answer",          default: "[No answer given]"
    t.boolean  "correct",         default: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["question_id"], name: "index_given_answers_on_question_id"
    t.index ["quiz_session_id"], name: "index_given_answers_on_quiz_session_id"
    t.index ["student_id"], name: "index_given_answers_on_student_id"
  end

  create_table "online_lists", force: :cascade do |t|
    t.integer  "student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["student_id"], name: "index_online_lists_on_student_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string   "title"
    t.integer  "row_order"
    t.integer  "quiz_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["quiz_id"], name: "index_questions_on_quiz_id"
  end

  create_table "quiz_sessions", force: :cascade do |t|
    t.string   "access_key"
    t.integer  "current_question_index"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "quiz_id"
    t.integer  "phase",                  default: 0
    t.index ["access_key"], name: "index_quiz_sessions_on_access_key", unique: true
    t.index ["quiz_id"], name: "index_quiz_sessions_on_quiz_id"
  end

  create_table "quizzes", force: :cascade do |t|
    t.string   "title"
    t.datetime "unlocked_at"
    t.integer  "course_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["course_id"], name: "index_quizzes_on_course_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.string   "type"
    t.integer  "quiz_session_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["quiz_session_id"], name: "index_users_on_quiz_session_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
