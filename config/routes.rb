Rails.application.routes.draw do

  resources :given_answers
  get 'students/set_quiz_session'

  resources :given_answers

  #change the sign_in and sign_out routes to /login and /logout
  devise_for :users, path: '', path_names: { sign_in: 'login',
                                             sign_out: 'logout'},
             :controllers => {:registrations => 'registrations'}

  resources :quiz_sessions, :answers, :questions, :quizzes, :courses

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'teacher/quiz_start', to: 'pages#create_session', :as => :teacher_start_quiz
  get 'teacher/quiz_question', to: 'pages#show_question', :as => :teacher_question
  get 'teacher/quiz_question_result', to: 'pages#show_question_result', :as => :teacher_question_result
  get 'teacher/quiz_result', to: 'pages#show_quiz_result', :as => :teacher_quiz_result
  get 'teacher/end_quiz_teacher', to: 'pages#end_quiz_teacher', :as => :end_quiz_teacher
  get 'teacher/new_quiz', to: 'pages#show', :role => 'teacher', :page => 'new_quiz', :as => :new_teacher_quiz
  get 'teacher/new_course', to: 'pages#show', :role => 'teacher', :page => 'new_course', :as => :new_teacher_course
  get 'teacher/home', to: 'pages#show', :role => 'teacher', :page => 'home', :as => :teacher_home
  get 'teacher', to: 'pages#show', :role => 'teacher', :page => 'home'
  get 'teacher/delete_quiz', to: 'quizzes#destroy', :as => :teacher_delete_quiz

  get 'student/quiz_start', to: 'pages#join_session', :as => :student_start_quiz
  get 'student/end_quiz_student', to: 'pages#end_quiz_student', :as => :end_quiz_student
  get 'student/home', to: 'pages#show', :role => 'student', :page => 'home', :as => :student_home
  get 'student', to: 'pages#show', :role => 'student', :page => 'home'

  devise_scope :user do
    get 'teacher_registration/sign_up' => 'devise/registrations#new', :teacher => 'yes'
  end

  get ':role/:page', to: 'pages#show'

  get '/', to: 'pages#show', :as => :welcome_page

  match '*path', to: 'pages#show', via: :all

  mount ActionCable.server => '/cable'
end
