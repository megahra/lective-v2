class Quiz < ApplicationRecord
  belongs_to :course
  has_one :quiz_session, dependent: :destroy
  has_many :questions, inverse_of: :quiz, dependent: :destroy

  accepts_nested_attributes_for :questions, reject_if: :all_blank, allow_destroy: true

  validates :title, presence: true

  validate :questions do
    errors.add(:quiz, " should have at least one question") if self.questions.length <= 0
  end
end
