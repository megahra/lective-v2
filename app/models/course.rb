class Course < ApplicationRecord
  belongs_to :teacher
  has_many :students
  has_many :quizzes, inverse_of: :course, dependent: :destroy

  delegate :teachers, :students, to: :users

  accepts_nested_attributes_for :quizzes, reject_if: :all_blank, allow_destroy: true

  validates :title, presence: true
end
