class GivenAnswer < ApplicationRecord
  belongs_to :student
  belongs_to :quiz_session
  belongs_to :question
end
