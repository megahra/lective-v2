class QuizSession < ApplicationRecord
  belongs_to :quiz
  has_one :teacher
  has_many :students

  delegate :teacher, :student, to: :user

  def current_question
    logger.debug '[DEBUG] quiz session model - method current_question entered.'
    logger.debug "[DEBUG] quiz session model - current_question_index: #{current_question_index}"
    current_question_index ? self.quiz.questions.sort_by(&:row_order)[current_question_index] : nil
  end

  def started?
    self.current_question.present?
  end

  # Switch to next question
  # Return false if it can't switch because it's the final question
  def switch_to_next_question!
    raise 'Quiz has to be started to switch the question' if !started?
    next_question_index = self.current_question_index+1
    next_question_exist = self.quiz.questions.sort_by(&:row_order)[next_question_index]
    logger.debug "[DEBUG] B - quiz session model - switch_to_next_question - is there a next question? => #{!next_question_exist.nil?}"
    if !next_question_exist.nil?
      self.current_question_index = next_question_index
      self.save
      true #true = yes there was a next question and I've incremented the current_question_index to point to it
    else
      false #False because there are no more questions
    end
  end

  def find_current_page(student)
    case self.phase
      when 1
        IndividualUpdateChannel.resend_answer_options(student)
      when 2 #ToDo: check if GivenAnswer for current question exists
    #    #IndividualUpdateChannel.resend_answer_options_after_selection(student)
      when 3
    #    IndividualUpdateChannel.resend_question_result(student)
      else # when 4
    #    IndividualUpdateChannel.resend_quiz_result(student)
    end
  end

  def render_student_start_quiz
    if File.exist?(Pathname.new(Rails.root + 'app/views/pages/student/quiz_start.html.erb'))
      render template: 'pages/student/quiz_start'
    else
      render file: 'public/404.html', status: :not_found
    end
  end

  def calc_chart_height
    given_answers = GivenAnswer.where(quiz_session_id: self.id, question_id: self.current_question.id).where.not(answer: '[No answer given]')
    counts = Hash.new 0
    given_answers.each do |given_answer|
      counts[given_answer.answer] += 1
    end
    if counts.length > 0 and counts.length < 2
      (counts.values.max)+2
    elsif counts.length >= 2
      (counts.values.max)
    else
      3
    end
  end

  #function used for bar_chart
  def parse_given_answers
    answer_occurrences = []
    answers = Question.find(self.current_question.id).answers
    answer_titles = []

    abcd = ['A: ', 'B: ', 'C: ', 'D: ']
    given_answers = GivenAnswer.where(quiz_session_id: self.id, question_id: self.current_question.id).group(:answer).count

    answers.each_with_index do |answer, index|
      #Append A: , B: , C: , D:
      answer_titles.push((abcd[index]).to_str + (answer.title).to_str)
      #save data pair: [title, num_answers]
      answer_occurrences.push([answer_titles[index], given_answers[answer.title]])
    end

    logger.debug "answer entries: #{answer_occurrences.inspect}"

    [{ data: {answer_titles[0] => answer_occurrences[0][1]}},
      { data: {answer_titles[1] => answer_occurrences[1][1]}},
      { data: {answer_titles[2] => answer_occurrences[2][1]}},
      { data: {answer_titles[3] => answer_occurrences[3][1]}}  ]
  end

  def calculate_colors
    color_array = []
    answers = Question.find(self.current_question.id).answers

    answers.each do |answer|
      if answer.correct
        color_array.push('lightgreen')
      else
        color_array.push('#e5e4e2')
      end
    end
    color_array
  end

  def calc_correct_answer
    answers = Answer.where(question_id: self.current_question.id)

    answers.each_with_index do | answer, index |
      if answer.correct
        case index
          when 0
            return 'A'
          when 1
            return 'B'
          when 2
            return 'C'
          else #when 3
            return 'D'
        end
      end
    end
  end

  #functions used for question result
  def calc_correct_answer_count
    GivenAnswer.where(question_id: self.current_question.id, quiz_session_id: self.id, correct: true).length
  end

  def calc_incorrect_answer_count(question_id)
    GivenAnswer.where(question_id: question_id, quiz_session_id: self.id, correct: true).length - GivenAnswer.where(question_id: question_id, quiz_session_id: self.id).length
  end

  def calc_given_correct_answers_of(question_id)
    GivenAnswer.where(question_id: question_id, quiz_session_id: self.id, correct: true).length
  end

  def calc_given_answers_of(question_id)
    GivenAnswer.where(question_id: question_id, quiz_session_id: self.id).length
  end

  def calc_answer_percent_of(question_id)
    if calc_given_answers_of(question_id) > 0
      ((calc_given_correct_answers_of(question_id).to_f/calc_given_answers_of(question_id).to_f) * 100).round(0)
    else
      0
    end
  end

  def calc_no_answer_given_count
    GivenAnswer.where(question_id: self.current_question.id, quiz_session_id: self.id, answer: '[No answer given]').length
  end

  def calc_given_answer_count
    GivenAnswer.where(question_id: self.current_question.id, quiz_session_id: self.id).where.not(answer: '[No answer given]').length
  end

  def calc_answer_percent
    if calc_given_answer_count > 0
      ((calc_correct_answer_count.to_f/calc_given_answer_count.to_f) * 100).round(0)
    else
      0
    end
  end

  def calc_no_answer_given_percent
    if calc_given_answer_count > 0
      ((calc_no_answer_given_count.to_f/calc_given_answer_count.to_f) * 100).round(0)
    else
      0
    end
  end

  def round(digits)
    f = (10 ** digits).to_f
    (self*f).round/f
  end

  #functions used for quiz result
  def quiz_result_data
    quiz_result = Hash.new
    quiz_questions = Question.where(quiz_id: self.quiz.id)

    quiz_questions.each_with_index do |question, index|
      quiz_result[(index+1).to_s + ': ' + question.title] = ((calc_correct_ga_of_question(question.id).to_f/calc_given_answers_of_question(question.id).to_f)*100)
    end

    quiz_result
  end

  def calc_given_answers_of_question(question_id)
    GivenAnswer.where(quiz_session_id: self.id, question_id: question_id).length
  end

  def calc_correct_ga_of_question(question_id)
    GivenAnswer.where(quiz_session_id: self.id, question_id: question_id, correct: true).length
  end

  #functions used for quiz result top three
  def calc_top_three
    top_three = Hash.new
    calc_users_correct_given_a.first(3).each { |k, v| top_three[k.name] = v.round}
    top_three
  end

  def calc_users_correct_given_a
    users_with_correct_answers = Hash.new 0
    given_answers = GivenAnswer.where(quiz_session_id: self.id, correct: true)
    current_time = Time.now

    given_answers.each do |given_answer|
      time = (given_answer.created_at - current_time) * 0.0000000000001
      users_with_correct_answers[User.find(given_answer.student_id)] += 1+time
    end

    users_with_correct_answers.sort_by {|k,v| v}.reverse
  end

  def calc_most_diff_question
    most_diff_question = nil
    lowest_value = nil
    all_questions = Question.where(quiz: self.quiz.id)

    all_questions.each do |question|
      logger.debug "======= #{question.id}"
      tmp = calc_incorrect_answer_count(question.id)
      logger.debug "======= #{tmp}"
      if lowest_value.nil? || tmp < lowest_value
        most_diff_question = question
        lowest_value = tmp
        logger.debug "======= current lowest_value #{tmp}"
      end
    end

    most_diff_question
  end

end
