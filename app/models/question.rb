class Question < ApplicationRecord
  belongs_to :quiz
  has_many :answers, inverse_of: :question, dependent: :destroy
  has_many :given_answers

  accepts_nested_attributes_for :answers, reject_if: :all_blank, allow_destroy: true

  validates :title, presence: true

  validate :answers do
    errors.add('', "should have exactly four answers") if self.answers.length != 4
    errors.add('', "should have only one correct answer") if self.answers.where(correct: true).size > 1
    errors.add('', "should have a correct answer") if answers.reject {|answer| !answer.correct? }.size < 1
  end
end
