class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable#,
         #:recoverable
  belongs_to :quiz_session, optional: true
  has_many :courses, :foreign_key  => "teacher_id" or "student_id" #temporary solution for relationships between teachers/students and courses

  validates :name, length: {minimum: 4, maximum: 12}, presence: true, uniqueness: false

  # Which users subclass the User model
  def self.types
    %w(Teacher Student)
  end

  # Add scopes to the parent models for each child model
  scope :teachers, -> { where(type: 'Teacher') }
  scope :students, -> { where(type: 'Student') }
end
