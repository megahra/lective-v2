class QuizDataBroadcastJob < ApplicationJob
  queue_as :default

  def perform(answers)
    ActionCable.server.broadcast('quiz_data', {
        page_html: render_page,
        answer_html: render_answer_options(answers)
    })
  end

  private
  def render_page
    ApplicationController.render(
        partial: 'pages/student/quiz_question',
        locals: {}
    )
  end

  def render_answer_options(answers)
    answers.each do |answer|
      ApplicationController.render(
          #render student/quiz_question page and render as many answer_option partials as needed
          partial: 'pages/student/answer_option',
          locals: {answer: answer}
      )
    end
  end
end
