class PagesController < ApplicationController
  before_action :get_variables, only: [:show_question, :start_quiz, :show_question_result, :show_quiz_result]
  #after_action :render_page, only: [:show]

  $error_403 = ['403 Forbidden', 'Hey, you. Yes, you! You are not allowed here. Scram!']
  $error_404 = ['Error 404', 'You are sad. I am sad. We are all sad.']

  def flash_403
    flash[:alert] = $error_403.join('<br/>').html_safe
  end

  def flash_404
    flash[:alert] = $error_404.join('<br/>').html_safe
  end

  def show
    if params[:role] == 'teacher' && params[:page] == 'home'
      if signed_in?
        @courses = current_user.courses
        # Order @courses by Semester DESC:
        #@courses = @courses.all.order("SUBSTR(semester, 3, 2) DESC, SUBSTR(semester, 1, 2) ASC")
      end
    end
    render_page(params[:role], params[:page])
  end

  #Teacher starts a quiz
  def create_session
    if current_user.nil?
      flash_403
      render_welcome
      return
    end
    if !current_user.quiz_session.present?
      @quiz_session = QuizSession.new(access_key: SecureRandom.hex(4), quiz_id: params[:quiz])
      if @quiz_session.save
        #add quiz session to current user
        current_user.quiz_session_id = @quiz_session.id
        if current_user.save!
          @quiz = Quiz.find(@quiz_session.quiz_id)
          params[:access_key] = @quiz_session.access_key
          ActionCable.server.broadcast(specific_channel, type: 'students_counter', counter: OnlineStatusChannel.count_unique_connections(current_user.id))
          render template: 'pages/teacher/quiz_start'
        else
          redirect_to_back('teacher/home', 'Could not save session for current user.')
        end
      else
        redirect_to_back('teacher/home', 'Could not create session.')
      end
    else
      @quiz_session = QuizSession.find(current_user.quiz_session_id)
      @quiz = Quiz.find(@quiz_session.quiz_id)
      render template: 'pages/teacher/quiz_start'
    end
    @current_question = @quiz_session.current_question
  end

  #Student joins a quiz
  def join_session
    if current_user.nil?
      flash_403
      render_welcome
    elsif current_user.type.downcase == 'teacher'
      flash_403
      render template: 'pages/teacher/home'
      return
    elsif current_user.type.downcase == 'student'
      access_key = params[:access_key]
      #Was anything in the text field?
      if access_key.present?
        set_access_key(access_key)
      elsif current_user.quiz_session.present?
        set_access_key(current_user.quiz_session.access_key)
      else
        flash[:alert] = 'No access_key found.'
        render template: 'pages/student/home'
      end
    end
  end

  def set_access_key(access_key)
    #Is it an existing quiz session?
    access_key_in_db = QuizSession.find_by access_key: access_key
    if access_key_in_db.present?
      #Is there a currently logged in student?
      if current_user.type == 'Student'
        if access_key_in_db.phase >= 0
          set_quiz_session(access_key)
          show_student_quiz_start
        else
          redirect_to_back('/', 'Access key not valid. Quiz has already ended.')
        end
      else
        redirect_to_back('/', 'Current user not a Student.')
      end
    else
      redirect_to_back('student/home', 'Access key not valid. Please check for typos.')
    end
  end

  def set_quiz_session(access_key)
    access_key = access_key
    unless  (access_key =~ /^\h{8}$/)
      flash[:alert] = 'Must be a valid access key.'
    else
      quiz_session = QuizSession.find_by access_key: access_key
      current_user.quiz_session_id = quiz_session.id
      if current_user.save!
        generate_given_answers(current_user)
      else
        flash[:alert] = 'Quiz session could not be joined.'
      end
    end
  end

  def generate_given_answers(student)
    #make and save a GivenAnswer record for each question of the current quiz
    quiz_session = QuizSession.find(student.quiz_session.id)
    questions = Quiz.find(quiz_session.quiz.id).questions

    questions.each do |question|
      #creates a GivenAnswer object with db default values (correct = false, answer = "[No Answer given.]")
      answer = GivenAnswer.where(student_id: student.id, quiz_session_id: quiz_session.id, question_id: question.id).last
      if !answer.blank?
        answer.update_attributes(answer: "[No answer given]", correct: false)
      else
        GivenAnswer.create!(student_id: student.id, quiz_session_id: quiz_session.id, question_id: question.id)
      end
    end
  end

  #Teacher presses start quiz button
  def start_quiz
    #Set current_question_index to 0:
    @quiz_session.current_question_index = 0
    @quiz_session.phase = 0
    if @quiz_session.save!
      #Resetting cqi was successful - proceed with quiz
      render_page('teacher', 'quiz_question')
    else
      redirect_to_back('teacher/quiz_start', 'Could not start quiz.')
    end
  end

  def show_student_quiz_start
    if get_variables
      render_page('student', 'quiz_start')
      reconnect_student
    else redirect_to_back('student/home', 'Could not find quiz session and/or quiz.')
    end
  end

  #Teacher presses next button (to reveal the question result)
  def show_question_result
    #show question result on teacher screen
    is_authorized = render_page('teacher', 'quiz_question_result')
    #show individual result for each student
    if is_authorized
      IndividualUpdateChannel.send_question_result(current_user)
    end
  end

  #Teacher presses quiz result button
  def show_quiz_result
    is_authorized = render_page('teacher', 'quiz_result')
    if is_authorized
      IndividualUpdateChannel.send_quiz_result(current_user)
    end
  end

  #Teacher presses next question button
  def switch_to_next_question
    #Is there another question?
    if @quiz_session.switch_to_next_question!
      render_page('teacher', 'quiz_question')
      true
    else
      false
    end
  end

  def show_question
    #If the quiz just started
    if @quiz_session.phase == 0
      start_quiz
      QuizDataChannel.send_answers(current_user) #which sets the phase = 1
    elsif @quiz_session.phase == 1 #is only called if the teacher reconnects
      render_page('teacher', 'quiz_question')
    #next the teacher calls show_question_result, which sets the phase to 3
    elsif @quiz_session.phase == 3
      if switch_to_next_question #if there is a next question
        QuizDataChannel.send_answers(current_user)
      else #else, if there is no next question - show the quiz result
        show_quiz_result
      end
    else
      #Teacher is reconnecting, display current question
      render_page('teacher', 'quiz_question')
    end
  end

  def self.redirect_to_start_quiz(user)
    @quiz = user.quiz_session.quiz
    if File.exist?(Pathname.new(Rails.root + 'app/views/pages/student/quiz_start.html.erb'))
      #ApplicationController.render(template: 'pages/student/quiz_start', locals: {quiz: @quiz})
      render template: 'pages/student/quiz_start', item: @quiz
    else
      render file: 'public/404.html', status: :not_found
    end
  end

  #Teacher presses end quiz button
  def end_quiz_teacher
    if current_user.present?
      quiz_participants = Student.where(quiz_session_id: current_user.quiz_session_id)
      quiz_participants.each do |quiz_participant|
        IndividualUpdateChannel.resend_quiz_result(quiz_participant)
        ActionCable.server.remote_connections.where(current_user: User.find(quiz_participant.id)).disconnect
        quiz_participant.update_attributes(quiz_session_id: nil)
      end
      ActionCable.server.remote_connections.where(current_user: User.find(current_user.id)).disconnect
      # phase is set to -1, to indicate that quiz cannot be resumed.
      QuizSession.find(current_user.quiz_session_id).update_attributes(phase: -1)
      current_user.quiz_session_id = nil
      flash[:alert] = 'Quiz session could not be joined.' unless current_user.save!
    end
    redirect_to '/teacher/home'
  end

  #Teacher presses end quiz button
  def end_quiz_student
    if current_user.present?
      if current_user.quiz_session.present?
        ActionCable.server.broadcast("online_status_#{current_user.quiz_session.id}", type: 'unsubscribe' )
      end
      current_user.update_attributes(quiz_session_id: nil)
      flash[:alert] = 'Quiz session could not be removed.' unless current_user.save!
    end
    redirect_to '/student/home'
  end

  private
  def specific_channel
    if current_user.quiz_session.present?
      "online_status_#{current_user.quiz_session.id}"
    end
  end

  def reconnect_student
    phase = current_user.quiz_session.phase
    case phase
      when 0 #quiz not started yet
        student_start_quiz_path
      when 1
        IndividualUpdateChannel.resend_answer_options(current_user)
      when 2 #answers are shown
        IndividualUpdateChannel.resend_answer_options_after_selection(current_user)
      when 3 #result for question are shown
        IndividualUpdateChannel.send_question_result(current_user)
      else #quiz is over - result for quiz is shown
        IndividualUpdateChannel.send_quiz_result(current_user)
    end
  end

  private
  def valid_page?(role = nil, page = nil)
    File.exist?(Pathname.new(Rails.root + "app/views/pages/#{role}/#{page}.html.erb"))
  end

  def render_page(role = nil, page = nil)
    if current_user.present?
      current_user_type = current_user.type.downcase
      #necessary if current_user is teacher
      @courses = current_user.courses
      if role.nil? && page.nil?
        if request.path != '/'
          flash_404
        end
        redirect_to "/#{current_user_type}/home"
        return true
      end
      if current_user_type != role
        if valid_page? role, page
          flash_403
        else
          flash_404
        end
        redirect_to "#{current_user_type}/home"
        return false
      end
    else
      if role == 'teacher' || role == 'student'
        if valid_page? role, page
          flash_403
        else
          flash_404
        end
        render_welcome
        return false
      else
        if role.nil? && page.nil? && request.fullpath == '/'
          render_welcome
          return true
        end
      end
    end

    if valid_page? role, page
      render template: "pages/#{role}/#{page}"
      true
    else
      flash_404
      render_welcome
      false
    end
  end

  def render_welcome
    render template: 'pages/welcome.html.erb'
  end

  def get_variables
    if current_user.nil?
      flash_403
      render_welcome
      return
    end
    if current_user.quiz_session_id.present?
      quiz_session = QuizSession.find(current_user.quiz_session_id)
      @quiz_session = quiz_session
      @quiz = Quiz.find(@quiz_session.quiz_id)
    else
      flash[:alert] = 'Quiz cannot be started. No quiz session set.'
    end
  end

  def redirect_to_back(default = root_url, notice = 'Action unsuccessful.')
    if request.env['HTTP_REFERER'].present? and request.env['HTTP_REFERER'] != request.env['REQUEST_URI']
      flash[:notice] = notice
      redirect_back(fallback_location: default)
    else
      redirect_to default, error: 'Referer unavailable.'
    end
  end

end
