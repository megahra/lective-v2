class RegistrationsController < Devise::RegistrationsController

  attr_accessor :new_user_role

  helper_method :teacher_sign_up
  helper_method :student_sign_up


  def new
    super
  end

  # POST /resource
  def create
    # Solution for displaying Devise errors on the homepage found on:
    # http://stackoverflow.com/questions/4101641/rails-devise-handling-devise-error-messages
    super
  end

  def teacher_sign_up
    logger.debug "[DEBUG] Teacher"
    $new_user_role='Teacher'
    new_user_registration_path
  end

  def student_sign_up
    logger.debug "[DEBUG] Student"
    $new_user_role='Student'
    new_user_registration_path
  end

  def sign_up_params
    params.require(resource_name).permit(:email, :password, :password_confirmation, :name, :type)
  end

end