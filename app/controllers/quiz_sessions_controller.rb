class QuizSessionsController < ApplicationController
  before_action :set_quiz_session, only: [:show, :edit, :update, :destroy]

  #attr_accessor :access_key
  #attr_accessor :current_question_index
  #attr_accessor :quiz

  # GET /quiz_sessions
  # GET /quiz_sessions.json
  def index
    @quiz_sessions = QuizSession.all
  end

  # GET /quiz_sessions/1
  # GET /quiz_sessions/1.json
  def show
  end

  # GET /quiz_sessions/new
  def new
    @quiz_session = QuizSession.new
  end

  # GET /quiz_sessions/1/edit
  def edit
  end

  # POST /quiz_sessions
  # POST /quiz_sessions.json
  def create
    @quiz_session = QuizSession.new(quiz_session_params)

    respond_to do |format|
      if @quiz_session.save
        format.html { redirect_to @quiz_session, notice: 'Quiz session was successfully created.' }
        format.json { render :show, status: :created, location: @quiz_session }
      else
        format.html { render :new }
        format.json { render json: @quiz_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quiz_sessions/1
  # PATCH/PUT /quiz_sessions/1.json
  def update
    respond_to do |format|
      if @quiz_session.update(quiz_session_params)
        format.html { redirect_to @quiz_session, notice: 'Quiz session was successfully updated.' }
        format.json { render :show, status: :ok, location: @quiz_session }
      else
        format.html { render :edit }
        format.json { render json: @quiz_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quiz_sessions/1
  # DELETE /quiz_sessions/1.json
  def destroy
    @quiz_session.destroy
    respond_to do |format|
      format.html { redirect_to quiz_sessions_url, notice: 'Quiz session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quiz_session
      @quiz_session = QuizSession.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quiz_session_params
      params.require(:quiz_session).permit(:access_key, :current_question_index)
    end
end
