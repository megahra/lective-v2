class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  #before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :current_teacher, :current_student,
                :teacher_logged_in?, :student_logged_in?


  private
  def current_teacher
    @current_teacher ||= current_user if user_signed_in? and current_user.class.name == 'Teacher'
  end

  def current_student
    @current_student ||= current_user if user_signed_in? and current_user.class.name == 'Student'
  end

  def teacher_logged_in?
    @teacher_logged_in ||= user_signed_in? and current_teacher
  end

  def student_logged_in?
    @student_logged_in ||= user_signed_in? and current_student
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: [:access_key])
    devise_parameter_sanitizer.permit(:sign_up) do |user|
      user.permit(:name, :email, :password, :password_confirmation)
    end
  end

  def after_sign_in_path_for(resource)
    super(resource)
    if current_user.type == 'Teacher'
      teacher_after_sign_in_redirect
    elsif current_user.type == 'Student'
      # do nothing
      student_after_sign_in
    else
      welcome_page
    end
  end

  def teacher_after_sign_in_redirect
    #is a quiz_session running (= is this a reconnect?)
    if current_user.quiz_session.present?
      phase = current_user.quiz_session.phase
      case phase
        when 0 #quiz not started yet
          teacher_start_quiz_path
        when 1, 2 #question is shown
          teacher_question_path
        when 3 #result for question is shown
          teacher_question_result_path
        else #quiz is over - result for quiz is shown
          teacher_quiz_result_path
      end
    # if no quiz session is running, redirect to home
    else
      teacher_home_path
    end
  end

  def student_after_sign_in
    student_home_path
  end

end
