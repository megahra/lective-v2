class QuizzesController < ApplicationController
  before_action :set_quiz, only: [:show, :edit, :update, :destroy]

  #attr_accessor :title
  #attr_accessor :unlocked_at

  # GET /quizzes
  # GET /quizzes.json
  def index
    @quizzes = Quiz.all
  end

  # GET /quizzes/1
  # GET /quizzes/1.json
  def show
  end

  # GET /quizzes/new
  def new
    @quiz = Quiz.new
    1.times do
      question = @quiz.questions.build
      4.times { question.answers.build }
    end
  end

  # GET /quizzes/1/edit
  def edit
  end

  # POST /quizzes
  # POST /quizzes.json
  def create
    @quiz = Quiz.new(quiz_params)
    if @quiz.save
      redirect_to teacher_home_path, notice: 'Quiz was successfully created.'
    else
      flash[:alert] = @quiz.errors.full_messages.join(".\n ")
      redirect_back(fallback_location: teacher_home_path)
    end
  end

  # PATCH/PUT /quizzes/1
  # PATCH/PUT /quizzes/1.json
  def update
    if @quiz.update(quiz_params)
      redirect_to teacher_home_path, notice: 'Quiz was successfully updated.'
    else
      flash[:alert] = 'Quiz was not successfully updated.'
      redirect_back(fallback_location: teacher_home_path)
    end
  end

  # DELETE /quizzes/1
  # DELETE /quizzes/1.json
  def destroy
    @quiz.destroy
    redirect_to teacher_home_path, notice: 'Quiz was successfully deleted.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quiz
      @quiz = Quiz.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quiz_params
      params.require(:quiz).permit(:title, :course_id, questions_attributes: [:id, :title, :row_order, :quiz_id, :_destroy, answers_attributes: [:id, :title, :correct, :question_id, :_destroy]])
    end
end
