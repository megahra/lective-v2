App.quiz_data = App.cable.subscriptions.create "QuizDataChannel",

  connected: ->
    # Called when the subscription is ready for use on the server
    console.log "Connected to QuizDataChannel"
    #$(document).on 'click', '#question_result_button', (e) -> App.individual_update.send_question_result() #use this type of event listener for elements that don't exist yet
    #$(document).on 'click', '#quiz_result_button', (e) -> App.individual_update.send_quiz_result()
    #update students counter when someone connects
    App.online_status.update_students_counter()

  disconnected: ->
    # Called when the subscription has been terminated by the server
    console.log "disconnected"
    App.quiz_data.resend_current_phase()
    App.online_status.update_students_counter()

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    console.log "quiz data channel - received"
    answer_ids = data.answers[1]["ids"]
    answerHtml = data.answers[0]

    # This will replace your body with the page html string:
    $(document.body).empty().append(answerHtml)

    disableAllButtons = ->
      counter = 0
      for id in answer_ids
        do (id) ->
          $('#answer_button_'+ counter).attr("disabled", true)
          $('#answer_button_'+counter).css("color", "grey")
          counter = counter + 1

    #need to set listeners in seperate loop since they only work if the buttons already exist -.-*
    counter = 0
    for id in answer_ids
      do (id) ->
        #console.log "[AC] answer id: #{id} and counter: #{counter}"
        $('#answer_button_'+counter).on 'click', (e) -> App.answer_data.send_data(id)
        $('#answer_button_'+counter).on 'click', (e) -> disableAllButtons()
        $('#answer_button_'+counter).on 'click', (e) -> $(this).css("background-color","white")
        $('#answer_button_'+counter).on 'click', (e) -> $(this).css("color", "black")
        counter = counter+1

    App.online_status.update_students_counter()

  send_answers: ->
    @perform 'send_answers'
    console.log "[AC] send data called!"

  resend_current_phase: ->
    @perform 'resend_current_phase'