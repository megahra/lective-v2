App.online_status = App.cable.subscriptions.create "OnlineStatusChannel",

  connected: ->
    # Called when the subscription is ready for use on the server
    #$(document).ready(App.online_status.update_students_counter() )
    App.online_status.update_students_counter()

  disconnected: ->
    # Called when the subscription has been terminated by the server
    console.log('disconnect')
    App.cable.subscriptions.remove(this)
    @perform 'unsubscribed'

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    console.log "[AC] online status channel - received method #{data.type}"
    switch data.type
      when "students_counter" then App.online_status.show_students_counter(data)
      when "answers_counter" then App.online_status.show_answers_counter(data)
      when "unsubscribe" then App.online_status.disconnected
    #  else console.log "wrong data.type #{data.type}"

  show_students_counter: (data) ->
    val = data.counter-1 #-1 since the teacher is also counted, but we only want to count students
    console.log "students counter: #{val}"
    #update "number of students that are connected to this quiz"-counter:
    $('#students_counter').text(val)

  show_answers_counter: (data) ->
    val = data.counter
    #update "number of answers that students have answered (current question)
    $('#given_answers_counter').text(val)

  update_students_counter: ->
    #console.log "[AC] update_students_counter called"
    @perform 'update_students_counter'

  update_answers_counter: ->
    @perform 'update_answers_counter'


