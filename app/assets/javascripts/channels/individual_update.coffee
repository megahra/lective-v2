App.individual_update = App.cable.subscriptions.create "IndividualUpdateChannel",
  connected: ->
    # Called when the subscription is ready for use on the server
    console.log "Connected to IndividualUpdateChannel"

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    console.log "individual update channel - received called, data: #{data.phase}"

    #switch for which cycle phase the quiz is in - displays the received html and sets the according event listeners
    switch data.phase
      when 0 then App.individual_update.reconstruct_quiz_start(data)
      when 1 then App.individual_update.reconstruct_answer_buttons(data)
      when 2 then App.individual_update.reconstruct_answer_buttons_after_selection(data)
      when 3 then App.individual_update.change_css(data.question_result['number_of_answers'], data.question_result['counter'], data.question_result['correct'])
      when 3.5 then App.individual_update.reconstruct_question_result(data.question_result) #called when a student reconnects while the quiz is in phase 3 - displaying current question result
      when 4 then App.individual_update.show_quiz_result(data.quiz_result)
      when 4.5 then App.individual_update.show_quiz_result(data.quiz_result)

  reconstruct_quiz_start: (data) ->
    console.log "individual update channel - reconstruct_quiz_start called"
    # This will replace your body with the page html string:
    $(document.body).empty().append(data.html)

  reconstruct_answer_buttons: (data) ->
    console.log "individual update channel - reconstruct_answer_buttons called"
    answer_ids = data.answers[1]["ids"]
    answerHtml = data.answers[0]

    # This will replace your body with the page html string:
    $(document.body).empty().append(answerHtml)

    disableAllButtons = ->
      counter = 0
      for id in answer_ids
        do (id) ->
          $('#answer_button_'+ counter).attr("disabled", true)
          $('#answer_button_'+counter).css("color", "grey")
          counter = counter + 1

    #need to set listeners in seperate loop since they only work if the buttons already exist -.-*
    counter = 0
    for id in answer_ids
      do (id) ->
        $('#answer_button_'+counter).on 'click', (e) -> App.answer_data.send_data(id)
        $('#answer_button_'+counter).on 'click', (e) -> disableAllButtons()
        $('#answer_button_'+counter).on 'click', (e) -> $(this).css("background-color","white")
        $('#answer_button_'+counter).on 'click', (e) -> $(this).css("color", "black")
        counter = counter+1

  reconstruct_answer_buttons_after_selection: (data) ->
    console.log "individual update channel - reconstruct_answer_buttons_after_selection called"
    answer_ids = data.answers[1]["ids"]
    answerHtml = data.answers[0]

    # This will replace your body with the page html string:
    $(document.body).html(answerHtml)

    #need to disable all buttons
    counter = 0
    for id in answer_ids
      do (id) ->
        $('#answer_button_'+counter).attr("disabled", true)
        $('#answer_button_'+counter).on 'click', (e) -> $(this).css("background-color","white")
        $('#answer_button_'+counter).on 'click', (e) -> $(this).css("color", "black")
        counter = counter+1

    #grey out selected answer


  reconstruct_question_result: (question_result) ->
    console.log "individual update channel - reconstruct_question_result called"
    answers = question_result[0]
    number_of_answers = question_result[1]['number_of_answers']
    #find out in which place (1st, 2nd, 3rd or 4th) the previously clicked answer stands:
    selected_answer_index = question_result[1]['counter']
    correct = question_result[1]['correct']

    answerHtml = answers[0]

    # This will replace your body with the page html string:
    $(document.body).html(answerHtml)
    App.individual_update.change_css(number_of_answers, selected_answer_index, correct)

  change_css: (number_of_answers, selected_answer_index, correct) ->
    #disable all buttons and make them look inactive
    for counter in [0..number_of_answers-1]
      do (counter) ->
        $('#answer_button_'+ counter).attr("disabled", true)
        $('#answer_button_'+counter).css("color", "grey")

    console.log "#{selected_answer_index+1}. answer was klicked!"

    if selected_answer_index != -1
      $('#answer_button_'+selected_answer_index).css("color", "black")
      if correct == true
        #style selected answer green if correct and red if incorrect
        $('#answer_button_'+selected_answer_index).css("background-color","darkseagreen")
      else
        $('#answer_button_'+selected_answer_index).css("background-color","darksalmon")
        #$('#no-answer-selected-notice').append '[No answer given]'#.css("margin-left", "auto").css("margin-right", "auto")

  show_quiz_result: (quiz_result_html) ->
    console.log "individual update channel - quiz_result_html called"
    # This will replace your body with the page html string:
    $(document.body).html(quiz_result_html)





  #display_current_phase: ->
    #@perform 'display_current_phase'

  send_question_result: ->
    console.log "1 individual update channel - send question result called!"
    @perform 'send_question_result'

  resend_question_result: (student) ->
    @perform 'resend_question_result', student

  send_quiz_result: ->
    @perform 'send_quiz_result'
