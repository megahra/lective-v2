App.answer_data = App.cable.subscriptions.create "AnswerDataChannel",

  connected: ->
    # Called when the subscription is ready for use on the server
    console.log "answer_data connected"

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    console.log "[AC] answer received by student id: #{data.student_id}"
    #update teacher view "x of y students answered so far"
    #update student view: disable buttons, keep showing answers, visually differentiate between picked answer and rest (grey out?)
    App.online_status.update_answers_counter()

  send_data: (message) ->
    console.log "[AC] send answer data called?"
    @perform 'send_data', id: message
    console.log "[AC] send answer data message: #{message}"