class AnswerDataChannel < ApplicationCable::Channel

  def subscribed
    if current_user.type == 'Teacher'
      stream_from specific_channel
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    super
  end

  def send_data(data)
    logger.debug "[AC] answer channel - send data method entered"
    #find the answer record with the clicked answers id
    answer = Answer.find(data["id"])
    quiz_session = QuizSession.find(current_user.quiz_session_id)

    #update GivenAnswer with picked answer
    if GivenAnswer.find_by(student_id: current_user.id, quiz_session_id: quiz_session.id, question_id: quiz_session.current_question.id)
           .update_attributes(answer: answer.title, correct: answer.correct)
      logger.debug "student (id: #{current_user.id}) gave an answer for question #{quiz_session.current_question_index+1} (question id: #{quiz_session.current_question.id}): '#{answer.title}' (id: #{answer.id})"
      ActionCable.server.broadcast(specific_channel, student_id: current_user.id) #we just need the "someone gave an answer"-event so we can update the given_answers_counter, we don't need the data since it was already saved to the db :)
    else
      logger.debug "[AC] send data: Could not find/update GivenAnswer for question #{quiz_session.current_question_index+1} :("
    end

    #save to db that we are entering into a new phase (answer was given)
    quiz_session = current_user.quiz_session
    quiz_session.phase = 2

    if quiz_session.save!
    else raise "[AnswerDataChannel] QuizSession phase (1->2) could not be changed!"
    end
  end

  private
  def specific_channel
    if current_user.quiz_session.present?
      "answer_data_#{current_user.quiz_session.id}"
    end
  end

end
