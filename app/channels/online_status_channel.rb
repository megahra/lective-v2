class OnlineStatusChannel < ApplicationCable::Channel

  def subscribed
    if current_user.quiz_session.present?
      stream_from specific_channel
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    ActionCable.server.broadcast(specific_channel, type: 'students_counter', counter: OnlineStatusChannel.count_unique_connections(current_user.id) )
  end

  def update_students_counter
    ActionCable.server.broadcast(specific_channel, type: 'students_counter', counter: OnlineStatusChannel.count_unique_connections(current_user.id) )
  end

  def update_answers_counter
    ActionCable.server.broadcast(specific_channel, type: 'answers_counter', counter: current_user.quiz_session.calc_given_answer_count )
  end

  private
  def specific_channel
    logger.debug "current_user.quiz_session #{current_user.quiz_session}"
    if current_user.quiz_session.present?
      "online_status_#{current_user.quiz_session.id}"
    end
  end

  #ToDo: Count connections per room
  #Counts all unique users connected to the ActionCable server
  def self.count_unique_connections(current_user_id)
    current_quiz_session_id = User.find(current_user_id).quiz_session_id
    connected_users = []
    ActionCable.server.connections.each do |connection|
      if connection.current_user.present? && connection.current_user.quiz_session_id.present?
        if current_quiz_session_id == connection.current_user.quiz_session_id
          connected_users.push(connection.current_user.id)
        end
      end
    end
    return connected_users.uniq.length
  end
end
