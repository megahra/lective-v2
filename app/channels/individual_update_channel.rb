class IndividualUpdateChannel < ApplicationCable::Channel

  def subscribed
    #only stream from this channel if you are a student
    if current_user.type == 'Student'
      stream_from specific_channel
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end

  def self.resend_answer_options(student)
    Rails.logger.debug "Student (id:#{student.id}) just requested phase 1 (answer buttons)!"
    quiz_session = student.quiz_session

    new_answer_html = AnswerButtonCreation.new(quiz_session).create
    ActionCable.server.broadcast("student_##{student.id}", phase: 1, answers: new_answer_html)
  end

  def self.resend_answer_options_after_selection(student)
    Rails.logger.debug "Student (id:#{student.id}) just requested phase 2 (answer buttons after selection)!"
    quiz_session = student.quiz_session

    new_answer_html = AnswerButtonCreation.new(quiz_session).create

    ActionCable.server.broadcast("student_##{student.id}", phase: 2, answers: new_answer_html)
  end

  def self.send_question_result(user) #gets called by the Teacher
    Rails.logger.debug "[AC] Individual Update Channel called!"
    quiz_session = user.quiz_session
    students = quiz_session.students

    #get all the ids of the displayed answers
    answer_html = AnswerButtonCreation.new(quiz_session).create
    answer_ids = answer_html[1]["ids"]

    #save to db that we are entering into a new phase
    quiz_session.phase = 3
    if quiz_session.save!
    else raise "[IndividualUpdateChannel] QuizSession phase (2->3) could not be changed!"
    end

    #send in the personal room of every Student connected to this quiz session
    students.each do |student|
      new_result_vars = QuestionResultCreation.new(student, answer_ids).create
      ActionCable.server.broadcast("student_##{student.id}", phase: 3, question_result: new_result_vars)
      Rails.logger.debug "[AC] Individual Update Channel just sent User #{student.id} a page!"
    end

  end

  def self.resend_question_result(student)
    #send in the personal room of the Student that reconnected
    answer_html = AnswerButtonCreation.new(student.quiz_session).create
    answer_ids = answer_html[1]["ids"]
    new_result_vars = QuestionResultCreation.new(student, answer_ids).create
    new_result_html = [answer_html, new_result_vars]
    ActionCable.server.broadcast("student_##{student.id}", phase: 3.5, question_result: new_result_html)
    Rails.logger.debug "[AC] Individual Update Channel just resent User #{student.id} a page!"
  end

  def self.send_quiz_result(user) #gets called by the Teacher
    Rails.logger.debug "[AC] Individual Update Channel called!"
    quiz_session = user.quiz_session
    students = quiz_session.students

    quiz_session.phase = 4
    if quiz_session.save!
    else raise "[IndividualUpdateChannel] QuizSession phase (3->4) could not be changed!"
    end

    #send in the personal room of every Student connected to this quiz session
    students.each do |student|
      resend_quiz_result(student)
      #Rails.logger.debug "[AC] Individual Update Channel just sent User #{student.id} a page!"
    end
  end

  def self.resend_quiz_result(student)
    quiz_result_html = QuizResultCreation.new(student).create
    ActionCable.server.broadcast("student_##{student.id}", phase: 4, quiz_result: quiz_result_html)
  end

  private
  def specific_channel
    "student_##{current_user.id}"
  end
end
