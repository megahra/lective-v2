module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
      #ToDo: can tags be used to count server connections per room (=access_key) instead of total number of connections?
      logger.add_tags 'ActionCable', self.current_user
    end

    private
    def find_verified_user
      User.find_by(id: cookies.signed[:user_id])
      # reject_unauthorized_connection
    end
  end
end
