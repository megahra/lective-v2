class QuizDataChannel < ApplicationCable::Channel

  def subscribed
    #only stream from this channel if you are a student
    if current_user.type == 'Student'
      stream_from specific_channel
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end

  def self.send_answers(user)
    Rails.logger.debug "[AC] send data method entered"
    quiz_session = user.quiz_session

    quiz_session.phase = 1
    if quiz_session.save!
    else raise "[QuizDataChannel] QuizSession phase (0->1) could not be changed!"
    end

    new_answer_html = AnswerButtonCreation.new(quiz_session).create
    ActionCable.server.broadcast("quiz_data_#{quiz_session.id}", phase: 1, answers: new_answer_html)

    Rails.logger.debug "[AC] Quiz Data Channel - Data created: #{new_answer_html[1].inspect}"

    Rails.logger.debug "[AC] data was broadcast"
  end

  def resend_current_phase
    current_user.quiz_session.find_current_page(current_user)
  end

  private
  def specific_channel
    "quiz_data_#{current_user.quiz_session.id}"
  end
end
