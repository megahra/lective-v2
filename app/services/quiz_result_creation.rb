class QuizResultCreation

  def initialize(student)
    if student.present? && student.quiz_session.present?
      @quiz_session = QuizSession.find(student.quiz_session.id)
      #get all answers of this student from this quiz session
      @given_answers = GivenAnswer.where(student_id: student.id, quiz_session_id: @quiz_session.id)
      Rails.logger.debug "QuestionResultCreation - GivenAnswers: #{@given_answers.inspect}"
    end
  end

  def create
    create_quiz_result_html
  end

  private
  def calculate_personal_score
    @correct_answer_num = 0
    if @given_answers.present?
      @given_answers.each do |answer| #.sort_by {|answer| answer.question.id}
        if answer.correct
          @correct_answer_num += 1
        end
      end
    end
    @correct_answer_num
  end

  def create_quiz_result_html
    page_html_top = "<div class='container-fluid'>
                      <div class='user-layout'>
                        <div class='row'>
                          <div class='col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1'>
                            <div class='question'>
                              <div class='jumbotron'>
                                <h2>Personal Quiz Result</h2></br>
                              </div>
                              <ul class='question-info'>
                                <li></li>
                                <li>"

    text_aborted = "<div>Quiz was cancelled.</div></br>"

    questions_count = 0
    if (@quiz_session.present? && @quiz_session.quiz.questions.present?)
      question_count = @quiz_session.quiz.questions.length.to_s
    end

    text = "<div>Your score: <span class='badge'>" + (calculate_personal_score).to_s + "</span> of <span class='badge'>" + question_count.to_s + "</span> questions answered correctly.</div></br>"

    page_html_bottom ="        </li>
                             </ul>
                            <div class='question-next-button'>
                              <a class='btn btn-primary btn-lg' href='/student/end_quiz_student'>Leave Quiz</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>"
    if @quiz_session.present? && @quiz_session.phase < 4
      page_html_top + text_aborted + page_html_bottom
    else
      page_html_top + text + page_html_bottom
    end
  end
end