class QuestionResultCreation

  def initialize(student, answer_ids)
    @student = student
    @quiz_session = student.quiz_session
    @question = @quiz_session.current_question
    @answer_ids = answer_ids
    @given_answer = GivenAnswer.where(student_id: @student.id, quiz_session_id: @quiz_session.id, question_id: @question.id).last

  end

  def create
    return create_vars_array
  end

  private
  def was_answer_correct?
    @given_answer.correct
  end

  def get_given_answer_id
    #ToDo: change GivenAnswer db schema to include Answer.id and find over relation
    selected_answer = Answer.find_by title: @given_answer.answer
    if selected_answer == nil
      Rails.logger.debug "QuestionResultCreation - get_given_answer_id: NO ANSWER MATCHED THE TEXT! => #{@given_answer.answer} (GivenAnswer id: #{@given_answer.id})"
      return -1
    else
      return selected_answer.id
    end
  end

  def get_given_answer_placement(answer_id)
    index = @answer_ids.index(answer_id)
    if index == nil
      Rails.logger.debug "QuestionResultCreation - get_given_answer_placement: NO ANSWER MATCHED THE ANSWER ID! => #{answer_id} not found in answer_ids: #{@answer_ids.inspect}!"
      return -1
    else
      return index
    end
  end

  def create_vars_array
    answer_id = get_given_answer_id
    @vars = {
        'number_of_answers' => @question.answers.length,
        'answer_id' => answer_id,
        'counter' => get_given_answer_placement(answer_id),
        'correct' => was_answer_correct?
    }
  end
end