class QuizDataCreation

  def initialize(user)
    @quiz_session = user.quiz_session
  end

  def create
    create_answer_data
  end

  private

  def create_answer_data

    @quiz_session.quiz.questions[@quiz_session.current_question_index].answers.map{|answer| answer.attributes.slice('id', 'title')}
  end

end