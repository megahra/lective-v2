class AnswerButtonCreation

  def initialize(quiz_session)
    @quiz_session = quiz_session
    @current_question_index = @quiz_session.current_question_index
  end

  def create
    answers_hash = get_answers
    return create_page_html(answers_hash), create_vars_array(answers_hash)
  end

  private
  def get_answers
    @quiz_session.quiz.questions[@current_question_index].answers.map{|answer| answer.attributes.slice('id', 'title')}
  end

  def slice_ids_from_answers(answers_hash)
    ids = []
    answers_hash.map do |hash|
      ids.push(hash["id"])
    end
    return ids
  end

  def create_answer_buttons(answers_hash)
    answer_buttons = String.new
    counter = 0
    answers_hash.each do |answer|
      answer_buttons += "<button type='button' id=#{'answer_button_' + counter.to_s} class='btn btn-primary btn-lg'>#{split_title_into_multiple_lines(answer["title"])}</button>"
      counter+=1
    end
    return answer_buttons
  end

  def split_title_into_multiple_lines(text)
    words = text.split(' ')
    button_text = String.new
    line = 1
    words.each do |word|
      button_text += word + ' '
      if button_text.length > 35*line
        button_text += '</br>'
        line += 1
      end
    end
    return button_text
  end

  def create_page_html(answers_hash)
    question_number = @current_question_index+1 #@current_question_index needs to be +1, since it's an array index and thus starts at 0
    page_html_top = "<div class='container-fluid'>
                <div class='student-layout'>
                  <div class='row'>
                    <div class='col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1'>
                      <div class='question'>
                        <div class='jumbotron'>
                          <h2>#{question_number}. Question</h2>
                        </div>
                        <div class='question-answers btn-group-vertical' role='group' id='answer-buttons'>"
                          #This is the point in the HTML where the answer buttons are inserted
    page_html_bottom =" </div>
                        <div id='no-answer-selected-notice'>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>"

    @page_html = page_html_top + create_answer_buttons(answers_hash) + page_html_bottom
  end

  def create_vars_array(answers_hash)
    @vars = {
        'ids' => slice_ids_from_answers(answers_hash),
        'current_question_index' => @current_question_index
    }
  end
end