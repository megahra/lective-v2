json.extract! course, :id, :title, :access_key, :semester, :created_at, :updated_at
json.url course_url(course, format: :json)
