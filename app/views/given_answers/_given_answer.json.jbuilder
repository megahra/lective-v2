json.extract! given_answer, :id, :student_id, :quiz_session_id, :question_id, :answer, :correct, :created_at, :updated_at
json.url given_answer_url(given_answer, format: :json)
