json.extract! quiz_session, :id, :access_key, :current_question_index, :created_at, :updated_at
json.url quiz_session_url(quiz_session, format: :json)
