require 'test_helper'

class QuizSessionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quiz_session = quiz_sessions(:one)
  end

  test "should get index" do
    get quiz_sessions_url
    assert_response :success
  end

  test "should get new" do
    get new_quiz_session_url
    assert_response :success
  end

  test "should create quiz_session" do
    assert_difference('QuizSession.count') do
      post quiz_sessions_url, params: { quiz_session: { access_key: @quiz_session.access_key, current_question_index: @quiz_session.current_question_index } }
    end

    assert_redirected_to quiz_session_url(QuizSession.last)
  end

  test "should show quiz_session" do
    get quiz_session_url(@quiz_session)
    assert_response :success
  end

  test "should get edit" do
    get edit_quiz_session_url(@quiz_session)
    assert_response :success
  end

  test "should update quiz_session" do
    patch quiz_session_url(@quiz_session), params: { quiz_session: { access_key: @quiz_session.access_key, current_question_index: @quiz_session.current_question_index } }
    assert_redirected_to quiz_session_url(@quiz_session)
  end

  test "should destroy quiz_session" do
    assert_difference('QuizSession.count', -1) do
      delete quiz_session_url(@quiz_session)
    end

    assert_redirected_to quiz_sessions_url
  end
end
